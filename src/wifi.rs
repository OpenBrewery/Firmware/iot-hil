use network_manager::NetworkManager;
use network_manager::DeviceType;
use network_manager::Device;
use network_manager::AccessPointCredentials;
use network_manager::Connection;
use network_manager::DeviceState;
use network_manager::errors::Result;
use network_manager::ConnectionState;

pub struct Wifi {
    device : Device,
}

impl Wifi {
    pub fn new() -> Wifi {
        NetworkManager::start_service(10).unwrap();
        let manager = NetworkManager::new();
        let devices = manager.get_devices().unwrap();
        let i = devices.iter().position(| ref d | * d.device_type() == DeviceType::WiFi).unwrap();
        Wifi {
            device : devices[i].to_owned(),
        }
    }

    pub fn disconnect(&mut self) -> Result<DeviceState> {
        self.device.disconnect()
    }

    pub fn create_hostpot(&mut self, ssid : &str, passwd : Option<&str>) -> Result<(Connection,ConnectionState)> {
        let manager = NetworkManager::new();
        let connection = manager.get_connections().unwrap();

        let conn = connection.iter()
        .find(|ref c| c.settings()
            .ssid
            .as_str()
            .unwrap() == ssid);
        match conn {
            Some(conn) => {
                conn.delete().unwrap();
            },
            None => {
                println!("Hotspot did not existed");
            }
        }
        let wifi_device = self.device.as_wifi_device().unwrap();
        wifi_device.create_hotspot(
            ssid,
            passwd,
            None,
        )
    }


    pub fn connect_to_ap(&mut self, ssid : &str, passwd : Option<&str>) -> Result<(Connection,ConnectionState)> {
        let wifi_device = self.device.as_wifi_device().unwrap();
        let access_points = wifi_device.get_access_points().unwrap();

        let matching_access_point = access_points.iter().find(|ap| ap.ssid.as_str().unwrap() == ssid);
        match matching_access_point {
            Some(matching_access_point) => {
                match passwd {
                    Some(passwd) => {
                        return wifi_device.connect(matching_access_point,&AccessPointCredentials::Wpa{passphrase : passwd.to_string()});
                    },
                    None => {
                        return wifi_device.connect(matching_access_point,&AccessPointCredentials::None);
                    }

                }
            },
            None => {
                return Err("SSID not present".into());
            }
        }
    }

}
