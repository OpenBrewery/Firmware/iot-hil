pub mod wifi;

#[test]
fn run(){
    let mut wifi_instance = wifi::Wifi::new();
    wifi_instance.disconnect().unwrap();
    wifi_instance.create_hostpot("TestHotspot",Some("Password")).unwrap();
    wifi_instance.connect_to_ap("TestHotspot",Some("Password")).unwrap();
    wifi_instance.disconnect().unwrap();

}